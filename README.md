R-Mol
=====

Remake of parts of [Jmol](http://jmol.sourceforge.net/) in rust.

Just a hobby project of mine to learn [rust](https://rust-lang.org) and [vulkan](https://www.khronos.org/vulkan/).

It uses:

+ [vulkano](https://github.com/tomaka/vulkano) for vulkan
+ [reqwest](https://github.com/seanmonstar/reqwest) for https GET
+ [rayon](https://github.com/nikomatsakis/rayon) to parallel download the SDF files

Parses [SDF](https://en.wikipedia.org/wiki/Chemical_table_file#SDF) files natively (just enough to get what it needs)

Usage
=====

```
 cargo build
 cargo run [<name of molecule or SDF file you downloaded yourself>]
```

Example using the web
=====================

```
 ./target/debug/rmol THC CBD morphine aspirin caffeine
```

Example using files
===================

```
 curl https://cactus.nci.nih.gov/chemical/structure/THC/sdf > THC.sdf
 curl https://cactus.nci.nih.gov/chemical/structure/CBD/sdf > CBD.sdf
 curl https://cactus.nci.nih.gov/chemical/structure/morphine/sdf > morphine.sdf
 ./target/debug/rmol *.sdf
```

Keyboard shortcuts
==================

+ space - cycle the molecules
+ d/D - change subdevision (make ball more or less ball like)
+ s/S - change scale (zoom in/out)
+ q - quit (also closing the window)

Missing
=======
+ Color - need to learn a bit more shader magic
+ Config file
+ Juxtaposition of molecules
+ Measuring distance
+ Atom labels
+ Spectra (like jspectra)
+ Html version - via the javascript / asm.js backend of llvm
