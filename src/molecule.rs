use std;
use std::fs::File;
use std::io::Read;
use std::collections::HashMap;

use cgmath::prelude::*;
use cgmath::Vector3;

use super::atom::{Atom, BondMultiplicity};
use super::sdf::SDFParser;
use super::object::{Object, WHITE};
use super::render::{render_sphere, render_tube};

pub struct Molecule
{
    pub name: String,
    atoms: Vec<Atom>,
    bonds: HashMap<(usize, usize), BondMultiplicity>,
}

fn from_vector3<T>(v: Vector3<T>) -> (T, T, T)
{
    (v.x, v.y, v.z)
}

impl Molecule
{
    pub fn sdf_url(molecule_name: &str) -> String
    {
        format!("https://cactus.nci.nih.gov/chemical/structure/{}/file?format=sdf", molecule_name)
    }

    pub fn from_sdf_file(filename: &str) -> Molecule
    {
        let mut contents = String::new();
        {
            let mut file = File::open(filename).unwrap(); // TODO - handle errors
            file.read_to_string(&mut contents).unwrap();
        }
        let name = std::path::Path::new(filename).file_stem().unwrap().to_str().unwrap();
        Molecule::from_sdf_text(name, &contents)
    }

    pub fn from_sdf_text(name: &str, contents: &str) -> Molecule
    {
        let results = SDFParser::parse(contents);
        println!("CREATED Molecule {}.", name);
        Molecule {
            name: String::from(name),
            atoms: results.atoms,
            bonds: results.bonds,
        }
    }

    pub fn render(&self, atoms: bool, bonds: bool, subdiv: usize) -> Object
    {
        if (!atoms || self.atoms.is_empty()) && (!bonds || self.bonds.is_empty())
        {
            return self.render_empty();
        }
        let mut obj = Object::new();
        let rfactor = 0.3f32;
        if atoms
        {
            for atom in &self.atoms
            {
                let new = render_sphere(atom.color(), atom.position, atom.radius() * rfactor, subdiv);
                obj.add_to_self(&new);
            }
        }
        if bonds
        {
            for (&(src, tgt), multi) in &self.bonds
            {
                //println!("{} ==({:?})==> {}", src, multi, tgt);
                let (asrc, atgt) = (&self.atoms[src], &self.atoms[tgt]);
                let pairs = match *multi {
                    BondMultiplicity::Single => {
                        vec![(rfactor, asrc.position, atgt.position)]
                    },
                    BondMultiplicity::Double => {
                        let (sx, sy, sz) = asrc.position;
                        let (tx, ty, tz) = atgt.position;
                        let src = Vector3::new(sx, sy, sz);
                        let tgt = Vector3::new(tx, ty, tz);
                        let min_r = asrc.radius().min(atgt.radius());
                        let dir = (tgt - src).normalize();
                        let up_sorta = Vector3 { x: 0.0f32, y: 1.0f32, z: 0.0f32 };
                        let side = dir.cross(up_sorta).normalize();
                        let d = side * min_r / 8.0f32;

                        vec![
                            (src + d, tgt + d),
                            (src - d, tgt - d)
                        ].iter().map(|&(a, b)| (rfactor / 2.0f32, from_vector3(a), from_vector3(b))).collect()
                    }
                };
                for (rfactor, start, end) in pairs {
                    let new = render_tube(start, end, 0.1f32, rfactor * asrc.radius(), rfactor * atgt.radius(), subdiv);
                    obj.add_to_self(&new);
                }
            }
        }
        println!("{}", obj);
        obj
    }

    fn render_empty(&self) -> Object
    {
        let (x, y, z) = (0.0f32, 0.0f32, 0.0f32);
        let radius = 0.5f32;
        render_sphere(WHITE, (x, y, z), radius, 64)
    }
}
