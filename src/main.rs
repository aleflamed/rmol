// Copyright (c) 2016 The vulkano developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.

extern crate cgmath;
extern crate winit;
extern crate time;
extern crate core;

use std::env;
use std::path::Path;

#[macro_use]
extern crate vulkano;
#[macro_use]
extern crate vulkano_shader_derive;
extern crate vulkano_win;

extern crate vulkano_text;

mod vulkanowin;
mod object;
mod curl;
mod render;
mod molecule;
mod atom;
mod sdf;
mod myeventloop;

use molecule::*;
use myeventloop::{MyHandler, Win, Handler};

fn get_molecules_from_filenames(filenames: &[String]) -> Vec<Molecule>
{
    filenames.iter().map(|name| Molecule::from_sdf_file(name)).collect()
}

fn get_molecules_from_web(names: Vec<String>) -> Vec<Molecule>
{
    let urls : Vec<String> = names.iter().map(|name| Molecule::sdf_url(name)).collect();
    curl::get_par(&urls).iter().zip(names).map(|(content, ref name)| Molecule::from_sdf_text(name, content)).collect()
}

fn read_molecules_from_command_line() -> Vec<Molecule>
{
    let mut filenames : Vec<String> = vec![];
    let mut names : Vec<String> = vec![];
    let args: Vec<String> = env::args().skip(1).collect();
    if args.is_empty()
    {
        return vec![];
    }
    for arg in args
    {
        if Path::new(&arg).exists()
        {
            filenames.push(arg);
        }
        else
        {
            names.push(arg);
        }
    }
    let mut from_file = get_molecules_from_filenames(&filenames);
    let from_web = get_molecules_from_web(names);
    from_file.extend(from_web);
    from_file
}

fn main() {
    let molecules = read_molecules_from_command_line();
    if molecules.is_empty()
    {
        println!("usage: {} [filename/molecule_name]+", env::args().nth(0).unwrap());
        return;
    }

    let mut win = vulkanowin::VulkanoWin::new();
    let (mut evloop, render_command) = MyHandler::new(molecules);
    win.enter_loop(&mut evloop as &mut Handler, render_command);
}
