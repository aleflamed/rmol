use core::f32::consts::PI;

use super::object::{Object, Vertex, Color, WHITE};

const TWO_PI: f32 = 2.0f32 * PI;

fn p(r: f32, alpha: f32, beta: f32) -> (f32, f32, f32)
{
    let (ca, sa, cb, sb) = (alpha.cos(), alpha.sin(), beta.cos(), beta.sin());
    (r * sb * ca, r * sb * sa, r * cb )
}

fn pv(r: f32, alpha: f32, beta: f32) -> Vertex
{
    Vertex { position: p(r, alpha, beta) }
}

fn vecsize(x: f32, y: f32, z: f32) -> f32
{
    (x * x + y * y + z * z).sqrt()
}

pub fn render_tube(start: (f32, f32, f32), end: (f32, f32, f32), r: f32, l_start: f32, l_end: f32, subdiv: usize) -> Object
{
    assert!(subdiv > 3);
    let (sx, sy, sz) = start;
    let (ex, ey, ez) = end;
    let (dx_un, dy_un, dz_un) = (ex - sx, ey - sy, ez - sz);
    let dist = vecsize(dx_un, dy_un, dz_un);
    let (dx, dy, dz) = (dx_un / dist, dy_un / dist, dz_un / dist);
    let beta = dx_un.atan2(-dy_un);
    let f_subdiv = subdiv as f32;
    let mut ret = Object::new();
    let start = Vertex { position: start };
    let dl = dist - l_start - l_end;
    let tube_color = WHITE;

    for ri in 0..subdiv
    {
        let alpha = TWO_PI / f_subdiv * (ri as f32);
        let alpha_plus = TWO_PI / f_subdiv * ((ri + 1) as f32);
        let alpha_mid = (alpha + alpha_plus) * 0.5f32;
        for li in 0..subdiv
        {
            let c = (li as f32) / f_subdiv * dl + l_start;
            let ddir = Vertex { position: (dx * c, dy * c, dz * c) };
            let c_plus = ((li + 1) as f32) / f_subdiv * dl + l_start;
            let ddir_plus = Vertex { position: (dx * c_plus, dy * c_plus, dz * c_plus) };
            let rad = vadd(&start, &pv(r, beta, alpha));
            let rad_plus = vadd(&start, &pv(r, beta, alpha_plus));
            let va = vadd(&ddir, &rad);
            let vb = vadd(&ddir, &rad_plus);
            let vc = vadd(&ddir_plus, &rad_plus);
            let vd = vadd(&ddir_plus, &rad);
            let n = pv(r, beta, alpha_mid);
            ret.add_square_abc_cda(va, vb, vc, vd, n, tube_color);
        }
    }
    ret
}

fn vadd(a: &Vertex, b: &Vertex) -> Vertex
{
    let &Vertex { position: (ax, ay, az) } = a;
    let &Vertex { position: (bx, by, bz) } = b;
    Vertex {
        position: (ax + bx, ay + by, az + bz)
    }
}

pub fn render_sphere(color: Color, center: (f32, f32, f32), radius: f32, subdiv: usize) -> Object
{
    assert!(subdiv > 3);
    // go over angles in both - not equal sized triangles, but easy to write
    let f_subdiv = subdiv as f32;
    // Note: this can be rewritten as three consecutive loops with no mutability, no idea if it would be better/worse performance.

    let center = Vertex { position: center };

    let mut ret = Object::new();
    for i in 0..subdiv
    {
        let alpha = TWO_PI / f_subdiv * (i as f32);
        let alpha_plus = TWO_PI / f_subdiv * ((i + 1) as f32);
        let alpha_mid = (alpha + alpha_plus) * 0.5f32;
        for j in 0..(subdiv/2)
        {
            let beta = TWO_PI / f_subdiv * (j as f32);
            let beta_plus = TWO_PI / f_subdiv * ((j + 1) as f32);
            let beta_mid = (beta + beta_plus) * 0.5f32;
            let va = vadd(&center, &pv(radius, alpha, beta));
            let vb = vadd(&center, &pv(radius, alpha_plus, beta));
            let vc = vadd(&center, &pv(radius, alpha_plus, beta_plus));
            let vd = vadd(&center, &pv(radius, alpha, beta_plus));
            let normal = pv(radius, alpha_mid, beta_mid);
            ret.add_square_abc_cda(va, vb, vc, vd, normal, color);
        }
    }
    ret
}
