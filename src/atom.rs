use super::object::{Color, RED, GREEN, BLUE, YELLOW, WHITE, ORANGE, PURPLE};

pub enum AtomType {
    Hydrogen,
    Oxygen,
    Nitrogen,
    Carbon,
    Sulphur,
    Phosphorus,
    Sodium,
}

impl AtomType
{
    pub fn from_letter(s: &str) -> AtomType
    {
        match s
        {
            "H" => AtomType::Hydrogen,
            "C" => AtomType::Carbon,
            "N" => AtomType::Nitrogen,
            "O" => AtomType::Oxygen,
            "S" => AtomType::Sulphur,
            "P" => AtomType::Phosphorus,
            "Na" => AtomType::Sodium,
            _ => panic!("unhandled {}", s)
        }
    }
}

pub struct Atom
{
    pub kind: AtomType,
    pub position: (f32, f32, f32)
}

impl Atom
{
    pub fn radius(&self) -> f32
    {
        match self.kind {
            AtomType::Hydrogen => 1f32,
            AtomType::Oxygen => 2f32,
            AtomType::Nitrogen => 1.8f32,
            AtomType::Carbon => 1.6f32,
            AtomType::Sulphur => 2.2f32,
            AtomType::Phosphorus => 2.2f32,
            AtomType::Sodium => 2.2f32,
        }
    }

    pub fn color(&self) -> Color
    {
        // space filling model convention, per Berg 8th ed. Pp 22
        match self.kind {
            AtomType::Hydrogen => WHITE,
            AtomType::Oxygen => RED,
            AtomType::Nitrogen => BLUE,
            AtomType::Carbon => GREEN, // BLACK really, but that's the background
            AtomType::Sulphur => YELLOW,
            AtomType::Phosphorus => ORANGE, // JMol uses this
            AtomType::Sodium => PURPLE,
        }
    }
}

#[derive(Debug)]
pub enum BondMultiplicity
{
    Single,
    Double
}
