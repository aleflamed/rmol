use std::cmp;

use time::precise_time_ns;

use cgmath::{Rad, Matrix4, Matrix3};

use winit::WindowEvent::{CursorMoved, MouseInput, ReceivedCharacter, CloseRequested};
use winit::Event::WindowEvent;
use winit::{Event, ElementState, MouseButton};
use winit::dpi::LogicalPosition;

use super::object::Object;
use super::molecule::Molecule;

struct Mouse
{
    click: (f64, f64),
    pos: (f64, f64),
    left_down: bool,
}

impl Mouse
{
    fn left(&mut self, down: bool)
    {
        if down {
            self.clicked();
        }
        self.left_down = down;
    }

    fn clicked(&mut self)
    {
        self.click = self.pos;
    }
    fn moved(&mut self, pos: (f64, f64)) { self.pos = pos; }

    fn change(&self) -> (f64, f64)
    {
        let (sx, sy) = self.click;
        let (x, y) = self.pos;
        (x - sx, y - sy)
    }

    fn left_drag(&self) -> (f64, f64)
    {
        if self.left_down {
            self.change()
        } else {
            (0f64, 0f64)
        }
    }
}

enum Command {
    ScaleFactor(f32),
    MouseMove(f64, f64),
    Left(bool),
    SwitchModel(usize),
    RenderAtoms(bool),
    RenderBonds(bool),
    Subdiv(usize),
    TimeToggle,
    Quit,
}

pub enum RenderCommand {
    Render(String, Object),
    World(Matrix4<f32>),
    ScaleFactor(f32),
    Quit,
}

pub trait Handler {
    fn handle_time(&mut self) -> Vec<RenderCommand>;
    fn handle_input(&mut self, ev: Event) -> Vec<RenderCommand>;
}

pub trait Win {
    fn enter_loop(&mut self, handler: &mut Handler, cmd: RenderCommand);
}

pub struct MyHandler
{
    obj_i: usize,

    time_rotation: bool,
    time_start: f32,
    angle_y: Rad<f32>,
    angle_z: Rad<f32>,

    molecules: Vec<Molecule>,
    subdiv: usize,
    render_atoms: bool,
    render_bonds: bool,

    mouse: Mouse,
}

impl MyHandler
{
    pub fn new(molecules: Vec<Molecule>) -> (MyHandler, RenderCommand)
    {
        let mut ret = MyHandler {
            obj_i : 0,
            molecules: molecules,

            time_rotation: false,
            time_start: 0.0f32,
            angle_y: Rad(0.0f32),
            angle_z: Rad(0.0f32),

            subdiv: 16,
            render_atoms: true,
            render_bonds: true,

            mouse: Mouse { click: (0f64, 0f64), pos: (0f64, 0f64), left_down: false },
        };
        let cmd = ret.load_object(0);
        (ret, cmd)
    }

    pub fn load_object(&mut self, i: usize) -> RenderCommand
    {
        let m = &self.molecules[i];
        let obj = m.render(self.render_atoms, self.render_bonds, self.subdiv);
        RenderCommand::Render(self.make_title(&m.name), obj)
    }

    fn make_title(&self, rest: &str) -> String
    {
        format!("rmol: {}", rest)
    }

    fn mouse_delta_to_angle(dx: f32, dy: f32) -> (Rad<f32>, Rad<f32>)
    {
        (Rad((dx as f32) * 0.01f32), Rad((dy as f32) * 0.01f32))
    }

    fn calc_world(&self) -> Matrix4<f32>
    {
        let (dx, dy) = self.mouse.left_drag();
        let (d_ang_y, d_ang_z) = MyHandler::mouse_delta_to_angle(dx as f32, dy as f32);
        let mat_y = Matrix3::from_angle_y(self.angle_y + d_ang_y);
        let mat_z = Matrix3::from_angle_z(self.angle_z + d_ang_z);
        Matrix4::from(mat_y * mat_z)
    }
}

impl Handler for MyHandler
{
    fn handle_time(&mut self) -> Vec<RenderCommand>
    {
        if !self.time_rotation {
            return vec![];
        }
        self.angle_y += Rad( {
            let new_t = precise_time_ns() as f32;
            let dt = (new_t - self.time_start) * 0.000000001;
            self.time_start = new_t;
            dt
        });
        vec![RenderCommand::World(self.calc_world())]
    }

    fn handle_input(&mut self, ev: Event) -> Vec<RenderCommand>
    {
        let mut ret : Vec<RenderCommand> = vec![];
        // avoid taking self into the closure, copy anything we need, change from closure, then copy back after
        let obj_i = self.obj_i;
        let n_molecules = self.molecules.len();
        let render_atoms = self.render_atoms;
        let render_bonds = self.render_bonds;
        let subdiv = self.subdiv;
        let set_molecule = |n: usize| {
            Command::SwitchModel(n % n_molecules)
        };
        let opt_cmd = match ev {
            WindowEvent { event: CloseRequested, .. } =>
                Some(Command::Quit),
            WindowEvent { event: CursorMoved { position: LogicalPosition { x, y }, .. }, .. } =>
                {
                    Some(Command::MouseMove(x, y))
                },
            WindowEvent { event: MouseInput{ state: ElementState::Released, button: MouseButton::Left, ..}, .. } =>
                {
                    println!("left release");
                    Some(Command::Left(false))
                }
                ,
            WindowEvent { event: MouseInput{ state: ElementState::Pressed, button: MouseButton::Left, .. }, .. } =>
                {
                    println!("left press");
                    Some(Command::Left(true))
                },
            WindowEvent { event: ReceivedCharacter(x), .. } =>
                {
                    match x {
                        'q' => Some(Command::Quit),
                        '[' => Some(set_molecule(obj_i + n_molecules - 1)),
                        ']'|' ' => Some(set_molecule(obj_i + 1)),
                        's' => Some(Command::ScaleFactor(1.0f32 / 1.1f32)),
                        'S' => Some(Command::ScaleFactor(1.1f32)),
                        'a' => Some(Command::RenderAtoms(!render_atoms)),
                        'b' => Some(Command::RenderBonds(!render_bonds)),
                        'd' => Some(Command::Subdiv(cmp::max(subdiv / 2, 4))),
                        'D' => Some(Command::Subdiv(cmp::min(subdiv * 2, 128))),
                        't' => Some(Command::TimeToggle),
                        '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
                            => Some(set_molecule((x as usize) - ('0' as usize))),
                        _ => { println!("received char {}", x); None }
                    }
                }
            _ => None //(println!("{:#?}", ev))
        };
        let mut render = false;
        if let Some(cmd) = opt_cmd {
            match cmd {
                Command::SwitchModel(model) => {
                    let molecule = &self.molecules[model];
                    let name = &molecule.name;
                    println!("model {}: {}", model, name);
                    self.obj_i = model;
                },
                Command::Left(down) => {
                    // apply drag to position
                    if !down {
                        let (dx, dy) = self.mouse.left_drag();
                        let (d_ang_y, d_ang_z) = MyHandler::mouse_delta_to_angle(dx as f32, dy as f32);
                        self.angle_y += d_ang_y;
                        self.angle_z += d_ang_z;
                    }
                    self.mouse.left(down);
                },
                Command::Quit => ret.push(RenderCommand::Quit),
                Command::ScaleFactor(factor) => ret.push(RenderCommand::ScaleFactor(factor)),
                Command::MouseMove(x, y) => {
                    self.mouse.moved((x, y));
                },
                Command::RenderAtoms(render_atoms) => { self.render_atoms = render_atoms; },
                Command::RenderBonds(render_bonds) => { self.render_bonds = render_bonds; },
                Command::Subdiv(subdiv) => { self.subdiv = subdiv; },
                Command::TimeToggle => {
                    self.time_rotation = !self.time_rotation;
                    self.time_start = precise_time_ns() as f32;
                },
            }
            render |= match cmd {
                Command::SwitchModel(_) | Command::RenderAtoms(_) | Command::RenderBonds(_) | Command::Subdiv(_) => true,
                _ => false
            };
        }
        let (diff_x, diff_y) = self.mouse.left_drag();
        if diff_x != 0.0 || diff_y != 0.0 {
            ret.push(RenderCommand::World(self.calc_world()));
        }
        if render {
            let obj_i = self.obj_i;
            ret.push(self.load_object(obj_i));
        }
        ret
    }
}
