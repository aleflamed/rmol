use std::collections::HashMap;

use super::atom::{Atom, BondMultiplicity, AtomType};

pub struct SDFParser
{
    pub atoms: Vec<Atom>,
    pub bonds: HashMap<(usize, usize), BondMultiplicity>,
}

impl SDFParser
{
    pub fn parse(contents: &str) ->Self
    {
        let mut res = SDFParser { atoms: vec![], bonds: HashMap::new() };

        // Skip first 4 lines - TODO
        // read all lines with 16 numbers. First three are position, then atom letter
        // skip the rest (the bonds) - TODO
        for (i, line) in contents.lines().enumerate()
        {
            if i < 4
            {
                continue
            }
            let words: Vec<&str> = line.split_whitespace().collect();
            if words.len() == 0 {
                continue
            }
            let ignore = words[0] == "M";
            match (ignore, words.len())
            {
                (false, 16) => res.add_atom(words),
                (false, 4) => res.add_atom_4(words),
                (false, 7) => res.add_bond(words),
                _ => println!("ignoring {} '{}'", words.len(), line)
            }
        }
        res
    }

    fn add_atom_4(&mut self, words: Vec<&str>)
    {
        self.add_atom_helper(words, 1, 2, 3, 0);
    }

    fn add_atom(&mut self, words: Vec<&str>)
    {
        self.add_atom_helper(words, 0, 1, 2, 3);
    }

    fn add_atom_helper(&mut self, words: Vec<&str>, x_i: usize, y_i: usize, z_i: usize, kind_i: usize)
    {
        let x = words[x_i].parse::<f32>().unwrap();
        let y = words[y_i].parse::<f32>().unwrap();
        let z = words[z_i].parse::<f32>().unwrap();
        let kind = AtomType::from_letter(words[kind_i]);
        let atom = Atom {
            position: (x, y, z),
            kind: kind
        };
        self.atoms.push(atom);
    }

    fn add_bond(&mut self, words: Vec<&str>)
    {
        let src = words[0].parse::<usize>().unwrap() - 1;
        let tgt = words[1].parse::<usize>().unwrap() - 1;
        assert!(src < self.atoms.len());
        assert!(tgt < self.atoms.len());
        let multiplicity = match words[2] {
            "1" => BondMultiplicity::Single,
            "2" => BondMultiplicity::Double,
            _ => panic!("unknown bond multiplicity: {}", words[2])
        };
        self.bonds.insert((src, tgt), multiplicity);
    }
}
