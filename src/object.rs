use core;

#[derive(Copy, Clone)]
pub struct Vertex {
    pub position: (f32, f32, f32)
}

impl_vertex!(Vertex, position);

#[derive(Copy, Clone)]
pub struct Normal {
    pub normal: (f32, f32, f32)
}

impl_vertex!(Normal, normal);

#[derive(Copy, Clone)]
pub struct Color {
    pub color: (f32, f32, f32)
}
impl_vertex!(Color, color);

//pub const GREY: Color = Color { color: (0.5, 0.5, 0.5) };
pub const WHITE: Color = Color { color: (1.0, 1.0, 1.0) };
pub const RED: Color = Color { color: (1.0, 0.0, 0.0) };
pub const GREEN: Color = Color { color: (0.0, 1.0, 0.0) };
pub const BLUE: Color = Color { color: (0.0, 0.0, 1.0) };
pub const YELLOW: Color = Color { color: (1.0, 1.0, 0.0) };
pub const ORANGE: Color = Color { color: (1.0, 0.5, 0.0) };
pub const PURPLE: Color = Color { color: (1.0, 0.0, 0.0) };


pub type Index = u32;

#[derive(Copy, Clone)]
pub struct PosColor {
    pub position: (f32, f32, f32),
    pub color: (f32, f32, f32)
}
impl_vertex!(PosColor, position, color);

pub struct Object
{
    pub vertices: Vec<PosColor>,
    pub normals: Vec<Normal>,
    pub indices: Vec<Index>,
}

impl core::fmt::Display for Object
{
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result
    {
        write!(f, "Object V {} N {} I {}", self.vertices.len(), self.normals.len(), self.indices.len())
    }
}

impl Object
{
    pub fn new() -> Object
    {
        Object {
            vertices: vec![],
            normals: vec![],
            indices: vec![],
        }
    }

    pub fn add_square_abc_cda(&mut self,
                          pos_a: Vertex,
                          pos_b: Vertex,
                          pos_c: Vertex,
                          pos_d: Vertex,
                          normal: Vertex,
                          color: Color,
    ) -> &mut Object
    {
        let Color { color: (colx, coly, colz) } = color;
        let n = self.vertices.len() as Index;
        let Vertex { position: (ax, ay, az)} = pos_a;
        let Vertex { position: (bx, by, bz)} = pos_b;
        let Vertex { position: (cx, cy, cz)} = pos_c;
        let Vertex { position: (dx, dy, dz)} = pos_d;
        let va = PosColor { position: (ax, ay, az), color: (colx, coly, colz) };
        let vb = PosColor { position: (bx, by, bz), color: (colx, coly, colz) };
        let vc = PosColor { position: (cx, cy, cz), color: (colx, coly, colz) };
        let vd = PosColor { position: (dx, dy, dz), color: (colx, coly, colz) };
        let mut newv = vec![va, vb, vc, vd];
        self.vertices.append(&mut newv);
        // TODO: should have different normals for each vertex - this is the normal for the average
        let Vertex { position: (nx, ny, nz) } = normal;
        let normal = Normal { normal: (nx, ny, nz) };
        let mut newn = vec![normal, normal, normal, normal];
        self.normals.append(&mut newn);
        let mut newind = vec![n, n + 1, n + 2, n + 2, n + 3, n];
        self.indices.append(&mut newind);
        self
    }

    // TODO: turn into an operator
    pub fn add_to_self(&mut self, other: &Object) -> &mut Object
    {
        let n_vertices = self.vertices.len() as Index;
        let vertices : &Vec<PosColor> = other.vertices.as_ref();
        for &v in vertices {
            self.vertices.push(v);
        }
        let normals : &Vec<Normal> = other.normals.as_ref();
        for n in normals {
            self.normals.push(*n);
        }
        let indices : &Vec<Index> = other.indices.as_ref();
        for i in indices {
            self.indices.push(i + n_vertices);
        }
        self
    }
}
