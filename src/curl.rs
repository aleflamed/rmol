//#![deny(warnings)]
extern crate reqwest;
extern crate rayon;

use self::rayon::prelude::*;

use std::io::Read;

pub fn get(url: &str) -> Option<String> {
    let mut resp = reqwest::get(url).unwrap();
    if !resp.status().is_success()
    {
        return None;
    }

    let mut content = String::new();
    resp.read_to_string(&mut content).unwrap();
    Some(content)
}

pub fn get_par(urls: &[String]) -> Vec<String>
{
    urls.par_iter()
        .map(|url| (url.clone(), get(url)))
        .filter(|&(ref url, ref x)| {
            match *x {
                None => {
                    println!("no molecule matches name {}", url);
                    false
                }
                Some(_) => true
            }
        })
        .map(|(_url, x)| x.unwrap())
        .collect()
}
