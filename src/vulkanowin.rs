use std::sync::Arc;
use std::f32::consts::FRAC_PI_2;

use cgmath::{Rad, SquareMatrix, Matrix4, Vector3, Point3, perspective};

use winit;

use vulkano;
use vulkano::buffer::cpu_access::CpuAccessibleBuffer;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::sync::GpuFuture;
use vulkano::pipeline::GraphicsPipeline;
use vulkano::pipeline::viewport::Viewport;
use vulkano::framebuffer::{Subpass};
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::swapchain::Surface;
use vulkano_win;
use vulkano_win::{VkSurfaceBuild};
use winit::Window;
use winit::dpi::LogicalSize;

use vulkano_text::{DrawText, DrawTextTrait};

use super::myeventloop::{Win, RenderCommand, Handler};

use super::object::{Normal, Index, Object, PosColor};

pub struct ObjectBuffers
{
    pub vertex_buffer: Arc<CpuAccessibleBuffer<[PosColor]>>,
    pub normals_buffer: Arc<CpuAccessibleBuffer<[Normal]>>,
    pub index_buffer: Arc<CpuAccessibleBuffer<[Index]>>,
}

impl ObjectBuffers
{
    fn new(
        device: &Arc<vulkano::device::Device>,
        indices: &[Index],
        vertices: &[PosColor],
        normals: &[Normal],
    ) -> ObjectBuffers
    {
        let vertex_buffer = CpuAccessibleBuffer
                ::from_iter(Arc::clone(device), vulkano::buffer::BufferUsage::all(), vertices.iter().cloned())
                .expect("failed to create buffer");

        let normals_buffer = CpuAccessibleBuffer
                ::from_iter(Arc::clone(device), vulkano::buffer::BufferUsage::all(), normals.iter().cloned())
                .expect("failed to create buffer");

        let index_buffer = CpuAccessibleBuffer
                ::from_iter(Arc::clone(device), vulkano::buffer::BufferUsage::all(), indices.iter().cloned())
                .expect("failed to create buffer");

        ObjectBuffers {
            vertex_buffer: vertex_buffer,
            normals_buffer: normals_buffer,
            index_buffer: index_buffer,
        }
    }
}


pub struct VulkanoWin {
    instance: Arc<vulkano::instance::Instance>,
    events_loop: winit::EventsLoop,
    buffers: Option<ObjectBuffers>,

    queue: Option<Arc<vulkano::device::Queue>>,
    device: Option<Arc<vulkano::device::Device>>,

    surface: Arc<Surface<Window>>,

    // Similar for all Wins (Win is actually also the renderer), should be abstracted
    world: Matrix4<f32>,
    scale: f32,
    title: String,
}

impl VulkanoWin {
    fn load_object(&mut self, obj: Object)
    {
        self.buffers = Some(ObjectBuffers::new(
            &self.device.as_ref().unwrap(),
            &obj.indices,
            &obj.vertices,
            &obj.normals));
    }

    fn set_title(&mut self, title: &str)
    {
        self.title = String::from(title);
        self.surface.window().set_title(&title);
    }

    fn do_command(&mut self, cmd: RenderCommand) -> bool
    {
        let mut ret = false;
        match cmd {
            RenderCommand::ScaleFactor(f) => self.scale *= f,
            RenderCommand::World(world) => self.world = world,
            RenderCommand::Render(title, obj) => {
                self.load_object(obj);
                self.set_title(&title);
            },
            RenderCommand::Quit => ret = true,
        }
        ret
    }
}

impl Win for VulkanoWin {

    fn enter_loop(&mut self, handler: &mut Handler, cmd: RenderCommand)
    {
        let instance = self.instance.clone();
        let physical = vulkano::instance::PhysicalDevice::enumerate(&instance)
                .next().expect("no device available");
        println!("Using device: {} (type: {:?})", physical.name(), physical.ty());

        let queue = physical.queue_families().find(|&q| q.supports_graphics() &&
                                                       self.surface.is_supported(q).unwrap_or(false))
                                                    .expect("couldn't find a graphical queue family");

        let device_ext = vulkano::device::DeviceExtensions {
            khr_swapchain: true,
            .. vulkano::device::DeviceExtensions::none()
        };

        let (device, mut queues) = vulkano::device::Device::new(physical, physical.supported_features(),
                                                                &device_ext, [(queue, 0.5)].iter().cloned())
                                   .expect("failed to create device");
        let queue = queues.next().unwrap();

        self.queue = Some(queue);
        self.device = Some(device);

        let device = self.device.as_ref().unwrap().clone();
        let queue = self.queue.as_ref().unwrap().clone();

        let vs = vs::Shader::load(Arc::clone(&device)).expect("failed to create shader module");
        let fs = fs::Shader::load(Arc::clone(&device)).expect("failed to create shader module");

        let mut dimensions;

        const DEFAULT_DIMENSIONS : [u32; 2] = [1280, 1024];

        let (mut swapchain, mut images) = {
            let caps = self.surface.capabilities(physical).expect("failed to get surface capabilities");

            dimensions = caps.current_extent.unwrap_or(DEFAULT_DIMENSIONS);
            let usage = caps.supported_usage_flags;
            let format = caps.supported_formats[0].0;

            vulkano::swapchain::Swapchain::new(
                device.clone(),
                Arc::clone(&self.surface),
                caps.min_image_count,
                format,
                dimensions,
                1,
                usage,
                &queue.clone(),
                vulkano::swapchain::SurfaceTransform::Identity,
                vulkano::swapchain::CompositeAlpha::Opaque,
                vulkano::swapchain::PresentMode::Fifo,
                true,
                None).expect("failed to create swapchain")
        };

        let renderpass = Arc::new(
            single_pass_renderpass!(Arc::clone(&device),
                attachments: {
                    color: {
                        load: Clear,
                        store: Store,
                        format: swapchain.format(),
                        samples: 1,
                    },
                    depth: {
                        load: Clear,
                        store: DontCare,
                        format: vulkano::format::Format::D16Unorm,
                        samples: 1,
                    }
                },
                pass: {
                    color: [color],
                    depth_stencil: {depth}
                }
            ).unwrap()
        );

        let pipeline = Arc::new(GraphicsPipeline::start()
            .vertex_input(vulkano::pipeline::vertex::TwoBuffersDefinition::new())
            .vertex_shader(vs.main_entry_point(), ())
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .fragment_shader(fs.main_entry_point(), ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(Arc::clone(&renderpass), 0).unwrap())
            .build(Arc::clone(&device))
            .unwrap());

                let proj = perspective(
            Rad(FRAC_PI_2),
            { let d = images[0].dimensions(); (d[0] as f32 / d[1] as f32) },
            0.01, 100.0);
        let view = Matrix4::look_at(
            Point3::new(0.3, 0.3, 1.0),
            Point3::new(0.0, 0.0, 0.0),
            Vector3::new(0.0, -1.0, 0.0));

        let uniform_buffer = vulkano::buffer::cpu_pool::CpuBufferPool::<vs::ty::Data>
            ::new(Arc::clone(&device), vulkano::buffer::BufferUsage::all());

        let depth_buffer = vulkano::image::attachment::AttachmentImage::transient(
            Arc::clone(&device), images[0].dimensions(), vulkano::format::D16Unorm).unwrap();

        let mut framebuffers = None;

        let mut draw_text = DrawText::new(device.clone(), queue.clone(), swapchain.clone(), &images);

        let LogicalSize { width, height } = self.surface.window().get_inner_size().unwrap();

        let mut previous_frame = Box::new(vulkano::sync::now(Arc::clone(&device))) as Box<GpuFuture>;

        let mut recreate_swapchain = false;

        // initial command - hack
        self.do_command(cmd);

        loop {
            previous_frame.cleanup_finished();

            if recreate_swapchain {
                let caps = self.surface.capabilities(physical).expect("failed to get surface capabilities");
                dimensions = caps.current_extent.unwrap_or(DEFAULT_DIMENSIONS);
                let (new_swapchain, new_images) = match swapchain.recreate_with_dimension(dimensions) {
                    Ok(r) => r,
                    Err(vulkano::swapchain::SwapchainCreationError::UnsupportedDimensions) => {
                        println!("failed to recreate swapchain with dimensions {:?}", dimensions);
                        continue;
                    },
                    Err(err) => panic!("{:?}", err)
                };
                swapchain = new_swapchain;
                images = new_images;
                framebuffers = None;
                recreate_swapchain = false;
            }

            if framebuffers.is_none() {
                framebuffers = Some(images.iter().map(|image| {
                    Arc::new(vulkano::framebuffer::Framebuffer::start(Arc::clone(&renderpass))
                        .add(Arc::clone(image)).unwrap()
                        .add(Arc::clone(&depth_buffer)).unwrap()
                        .build().unwrap())
                }).collect::<Vec<_>>());
            }

            let (image_num, acquire_future) =
                match vulkano::swapchain::acquire_next_image(Arc::clone(&swapchain), None) {
                    Ok(r) => r,
                    Err(vulkano::swapchain::AcquireError::OutOfDate) => {
                        recreate_swapchain = true;
                        println!("recreate swapchain");
                        continue;
                    }
                    Err(err) => panic!("{:?}", err)
                };

            let uniform_buffer_subbuffer = {
                let scale = Matrix4::from_scale(self.scale);
                let uniform_data = vs::ty::Data {
                    world : self.world.into(),
                    view : (view * scale).into(),
                    proj : proj.into(),
                };
                uniform_buffer.next(uniform_data).unwrap()
            };

            let set = Arc::new(PersistentDescriptorSet::start(Arc::clone(&pipeline), 0)
                .add_buffer(uniform_buffer_subbuffer).unwrap()
                .build().unwrap()
            );

            draw_text.queue_text(200.0, 50.0, 20.0, [1.0, 1.0, 1.0, 1.0], &self.title);

            {
                let buffers = &self.buffers.as_ref().unwrap();
                let viewports = vec![Viewport {
                                origin: [0.0, 0.0],
                                depth_range: 0.0 .. 1.0,
                                dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                            }];
                let command_buffer =
                    AutoCommandBufferBuilder::primary_one_time_submit(Arc::clone(&device), queue.family()).unwrap()
                    .begin_render_pass(
                        framebuffers.as_ref().unwrap()[image_num].clone(), false,
                        vec![
                            [0.0, 0.0, 0.0, 1.0].into(),
                            1f32.into()
                        ]).unwrap()
                    .draw_indexed(
                        Arc::clone(&pipeline),
                        &vulkano::command_buffer::DynamicState {
                            line_width: None,
                            viewports: Some(viewports),
                            scissors: None,
                        },
                        (Arc::clone(&buffers.vertex_buffer), Arc::clone(&buffers.normals_buffer)),
                        Arc::clone(&buffers.index_buffer),
                        Arc::clone(&set),
                        ()).unwrap()
                    .end_render_pass().unwrap()
                    .draw_text(&mut draw_text, image_num)
                    .build().unwrap();
                let future = previous_frame.join(acquire_future)
                    .then_execute(queue.clone(), command_buffer).unwrap()
                    .then_swapchain_present(queue.clone(), Arc::clone(&swapchain), image_num)
                    .then_signal_fence_and_flush().unwrap();
                previous_frame = Box::new(future) as Box<_>;
            }

            let mut commands : Vec<RenderCommand> = vec![];
            self.events_loop.poll_events(|ev| {
                for cmd in handler.handle_input(ev) {
                    commands.push(cmd);
                }
            });
            for cmd in handler.handle_time() {
                commands.push(cmd);
            }
            let mut quit = false;
            for cmd in commands {
                quit |= self.do_command(cmd);
            }
            if quit {
                break;
            }
        }
    }
}

impl VulkanoWin {
    pub fn new() -> VulkanoWin
    {
        let extensions = vulkano_win::required_extensions();

        let instance = vulkano::instance::Instance::new(None, &extensions, None).expect("failed to create instance");

        let events_loop = winit::EventsLoop::new();

        let surface = winit::WindowBuilder::new()
//            .with_title("rmol")
            .build_vk_surface(&events_loop, instance.clone()).unwrap();

        VulkanoWin {
            instance: instance,
            events_loop: events_loop,
            buffers: None,
            surface: surface,
            device: None,
            queue: None,
            world: Matrix4::identity(),
            scale: 0.1f32,
            title: String::from(""),
        }
    }
}

mod vs {
    #[derive(VulkanoShader)]
    #[ty = "vertex"]
    #[src = "
#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec3 normal;

layout(location = 0) out vec3 v_normal;
layout(location = 1) out vec3 f_color;

layout(set = 0, binding = 0) uniform Data {
    mat4 world;
    mat4 view;
    mat4 proj;
} uniforms;

void main() {
    mat4 worldview = uniforms.view * uniforms.world;
    vec3 n = transpose(inverse(mat3(worldview))) * vec3(normal.x, normal.y, normal.z);
    v_normal = vec3(n.x, n.y, n.z);
    gl_Position = uniforms.proj * worldview * vec4(position, 1.0);
    f_color = color;
}
"]
    struct _Dummy;
}

mod fs {
    #[derive(VulkanoShader)]
    #[ty = "fragment"]
    #[src = "
#version 450

layout(location = 0) in vec3 v_normal;
layout(location = 1) in vec3 v_color;
layout(location = 0) out vec4 f_color;

const vec3 LIGHT = vec3(0.0, 0.0, 1.0);

void main() {
    float brightness = dot(normalize(vec3(v_normal.x, v_normal.y, v_normal.z)), normalize(LIGHT));
    //vec3 dark_color = vec3(0.6, 0.0, 0.0);
    f_color = vec4(v_color, 1.0); // mix(dark_color, v_color, brightness)
}
"]
    struct _Dummy;
}
