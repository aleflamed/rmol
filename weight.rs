use std::io;
use std::collections::HashMap;

////////////////////////////////////////////////////////////////////////////////
// TODO: use a common library, not copy paste from src/atom.rs

#[derive(PartialEq,Eq,Hash)]
pub enum AtomType {
    Hydrogen,
    Oxygen,
    Nitrogen,
    Carbon,
    Sulphur,
    Phosphorus,
    Sodium,
}

impl AtomType
{
    pub fn from_symbol(s: &str) -> AtomType
    {
        match s
        {
            "H" => AtomType::Hydrogen,
            "C" => AtomType::Carbon,
            "N" => AtomType::Nitrogen,
            "O" => AtomType::Oxygen,
            "S" => AtomType::Sulphur,
            "P" => AtomType::Phosphorus,
            "Na" => AtomType::Sodium,
            _ => panic!("unhandled {}", s)
        }
    }

    pub fn molar_weight(&self) -> f32
    {
        // Source: Gchem and Kalzium (accurate to 0.01 gram)
        match *self
        {
            AtomType::Hydrogen => 1.00794f32,
            AtomType::Carbon => 12.0107f32,
            AtomType::Nitrogen => 14.0067f32,
            AtomType::Oxygen => 15.9994f32,
            AtomType::Sulphur => 32.065f32,
            AtomType::Phosphorus => 30.9738f32,
            AtomType::Sodium => 22.9898f32,
        }
    }
}

// End of include src/atom.rs
////////////////////////////////////////////////////////////////////////////////

struct Formula {
    parts: HashMap<AtomType, u32>
}

impl Formula {
    fn new(s_no_space: &String) -> Formula
    {
        let s = format!("{} ", s_no_space); // append space as an end symbol
        let mut hash : HashMap<AtomType, u32> = HashMap::new();
        enum State {
            Type,
            Count
        };
        let mut state = State::Type;
        let mut num_str = String::new();
        let mut symbol = String::new();
        for c in s.chars() {
            match state {
                State::Type => {
                    match c {
                        '0'...'9' => {
                            state = State::Count;
                            num_str = format!("{}", c);
                        }
                        o => {
                            symbol = format!("{}{}", symbol, o); // TODO - what's the fastest succintest way? c is a char
                        }
                    }
                },
                State::Count => {
                    match c {
                        '0'...'9' => {
                            num_str = format!("{}{}", num_str, c);
                        }
                        // " ", aka space, is added as an end symbol, so this will happen for the
                        // last as well
                        o => {
                            let count = num_str.parse().unwrap();
                            let a = AtomType::from_symbol(&symbol);
                            hash.insert(a, count);
                            state = State::Type;
                            symbol = format!("{}", o);
                        }
                    }
                }
            }
        }
        Formula { parts: hash }
    }

    fn molar_weight(&self) -> f32
    {
        let mut total = 0.0f32;
        for (kind, plurality) in &self.parts {
            total += (*plurality as f32) * kind.molar_weight();
        }
        total
    }
}

fn molar_weight(s: &String) -> f32
{
    return Formula::new(s).molar_weight();
}

fn main()
{
    loop {
        let mut formula = String::new();
        io::stdin().read_line(&mut formula).unwrap();
        if formula.is_empty() {
            break;
        }
        println!("{}", molar_weight(&formula));
    }
}
